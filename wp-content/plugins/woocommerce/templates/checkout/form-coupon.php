<?php
/**
 * Checkout coupon form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-coupon.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! wc_coupons_enabled() ) {
	return;
}


//mostrar despues de compratir con facebook
if ( ! WC()->cart->applied_coupons ) {
    $info_message = apply_filters( 'woocommerce_checkout_coupon_message', __( 'Have aas couponasd?', 'woocommerce' ) . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>' );
   //wc_print_notice( $info_message, 'notice' );
}
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<div id="fb-root"></div>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5&appId=1775425902677403";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<script>
    function compartir(){
                    FB.ui({
                            method: 'share',
                        href: 'https://www.facebook.com/Primal-World-913204918799158/?fref=ts',
                        },
                        // callback
                        function (response) {
                            if (response && !response.error_message) {
                                jQuery("#mostrar").show();
                            } else {
                                //alert('Error while posting.');
                            }
                        }
                    );
                
    }
</script>


<script>	
    function pene(){
        jQuery("#mostrar").show();
    }
    jQuery(document).ready(function() {	
        function click(){
            alert("");
        }
        //document.getElementById('mostrar2').css('background-color', '#ff0000');
    });
</script>


<br> 
<h4>Share Primal world on your Facebook wall or your Twitter timeline and get a <span style="font-size:25px"> 10% </span> discount</h4><br><br>

<div id="mostrar" class="checkout_coupon" style="display:none">S
    <form method="post" class="mostrar2" >

        <p class="form-row form-row-first">
            <input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="10%OFF" />
        </p>

        <p class="form-row form-row-last">
            <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />
        </p>

        <div class="clear"></div>
    </form>
</div>
