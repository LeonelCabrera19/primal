<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'primal');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1]$*B5_hSWNbl7rQd^l^@D?+iE+!O`azN),-X[w^jO].d_P JZ}DJ11C8!Vau^UV');
define('SECURE_AUTH_KEY',  'n^Pk{%MvKxu)iZZi coUy!yV[H;qq-v3lc;(XkfFhc)bs?rO).U:< *OzJG4IcuP');
define('LOGGED_IN_KEY',    'RP;g!%BDmG}}a`T*4C+Edtq/3 =&-$8qttlr,vVW{zhP~ L4rYw_T?!&V,=: Z.=');
define('NONCE_KEY',        'aQZb3X~_4#T@7jN|-adr>!kQL2)::y8eJ*ubk7^orh+qks?R35yL>WE3W]t]T/fG');
define('AUTH_SALT',        'im*|STxc$1ax3+Q*Sct9&hPScQ4k&BZmPe/|xq%U?.5s:?_O$WrKRJYUe~z$b}m|');
define('SECURE_AUTH_SALT', '%_aY`POp1#`4,&0(P. hxCJl~_J+J1(KJZzNlg^|!nOvIfM/IBk9Rh+>+ 1~r8Lt');
define('LOGGED_IN_SALT',   '>]ZvC@h$[KVG,@O-8i@ iy:!VJXS^[&~w7uU^8Y{[J*[:J3RYGk)dgR} -Nv9?5S');
define('NONCE_SALT',       'v:|sY$]]!QyN7CsPwC;l2Vk+KcE&.K-byEhEAq3YA5C|Rg<tGV{wZ0y>v&h?A@-l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
